<!-- Update the lab title -->
# Lab 7 - Bonus Material

<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
### Virtual Cameras

Install NDI for webcam or OBS Virtual Webcam
- Use NDI sources
- Test on zoom, google meet, etc.
- using OBS + NDI across computers on the same network

Splitcam


### Virtual audio cables

paid, but worth it if you want to do more complex stuff with audio signals
zoom audio


### install touch portal or buy a streamdeck

can be nice for switching


### Restream.io

useful for combining multiple streaming services
