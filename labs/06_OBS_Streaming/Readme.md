<!-- Update the lab title -->
# Lab 6 - OBS Streaming

<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
### Sub-Header

ToDo - IF DESIRED:
- connect to streaming service
- 'go live' and start a live stream (ask someone from class to check it out!)
- change scenes while live
- try recording while streaming
