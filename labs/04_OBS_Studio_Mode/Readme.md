<!-- Update the lab title -->
# Lab 4 - OBS Studio Mode

<!-- Reminder: -->
<!-- Labs should take ~60 minutes -->
<!-- ~25% Talk: What and why. -->
<!-- ~25% Demo: What and how. -->
<!-- ~50% Do: Pair and share, redoing the demo -->

<!-- IF THIS IS THE LAST LAB, PUT FEEDBACK REMINDERS AT THE START -->

<!-- Update this sub-header -->
### Sub-Header

Using studio mode, and transitions, allows you to queue scenes up and make adjustments before sending it out to the stream or to the recording.

To Do:
- go into studio mode
- 'queue' up a scene before going live
- try different transitions
- transition delays?
- adjust sources in a scene (or nested scenes) and go live
